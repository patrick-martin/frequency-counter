document.getElementById('countButton').onclick = function() {
        let typedText = document.getElementById('textInput').value;
        typedText = typedText.toLowerCase();
        typedText = typedText.replace(/[^a-z'\s]+/g, "");

        const letterCount = {}; //Empty Object that we are building upon
        for (let i = 0; i < typedText.length; i++) { //Initializing loop to get individual letters of typedText
            currentLetter = typedText[i]; //Creating variable to store each letter
            // letterCount.push(currentLetter);   //Aha!  You cannot 'push' a new item into an object like an array
            //letterCount[currentLetter]; //This should add each individual letter into letterCount array

            if (letterCount[currentLetter] === undefined) { //Checks to see if typed letter property exists within letterCount object
                letterCount[currentLetter] = 1; //if letter isn't found, loop sets value to 1;
            } else {
                letterCount[currentLetter]++; //if loop finds letter within Object, it increments 1 to the letter
            }
            for (let letter in letterCount) { //using a for...in loop to get frequency of letters
                const letterSpan = document.createElement('span');
                const textContent = document.createTextNode('"' + letter + "\": " + letterCount[letter] + ", ");
                letterSpan.appendChild(textContent);
                document.getElementById('lettersDiv').innerHTML = '';
                document.getElementById('lettersDiv').appendChild(letterSpan);
            }
        };

        //Generate an empty object that will hold each word(key), as well as the frequency of each word(value)
        //Should output each key + value to the 'wordsDiv' HTML element when ran
        const wordCount = {};
        const words = typedText.split(" ");
        for (let i = 0; i < words.length; i++) {
            currentWord = words[i];
            wordCount[currentWord];

            if (wordCount[currentWord] === undefined) {
                wordCount[currentWord] = 1;
            } else {
                wordCount[currentWord]++;
            }
            for (let wordProp1 in wordCount) {
                const wordSpan = document.createElement('span');
                const wordContent = document.createTextNode('' + wordProp1 + ': ' + wordCount[wordProp1] + ',');
                wordSpan.appendChild(wordContent);
                document.getElementById('wordsDiv').innerHTML = '';
                document.getElementById('wordsDiv').appendChild(wordSpan);
            }
        }
    }
    // console.log(wordCount)
    // console.log(letterCount)